<?php

use Illuminate\Foundation\Application;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use Laravel\Passport\Exceptions\AuthenticationException;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {
        //
    })
    ->withExceptions(function (Exceptions $exceptions) {
        $exceptions->render(fn(ValidationException $e) => response()->json([ "message" => "There were some validation errors.", "errors" => $e->errors() ], 422));
        $exceptions->render(fn(AuthenticationException $e) => response()->json([ "message" => "No autenticado." ], 401));
        $exceptions->render(function(Throwable $e) {
            $message = "There was an error while processing your request.";

            if (config("app.env") !== "production") {
                $message = sprintf("%s (Reason: %s)", $message, $e->getMessage());
            }

            logger()->warning($e->getMessage(), $e->getTrace());
            return response()->json([ "message" => $message ], 422);
        });
    })->create();
