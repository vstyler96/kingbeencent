<?php

use Illuminate\Database\Migrations\Migration;
use Database\Seeders\ApplicationMaintenanceSeeder;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        (new ApplicationMaintenanceSeeder)->installPassportclients();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
