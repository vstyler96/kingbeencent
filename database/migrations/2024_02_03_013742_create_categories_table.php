<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            /**
             * A category can be be:
             * - A product category, for example:
             *   - Electronics.
             *   - Clothing.
             *   - Shoes.
             * - A service category, for example:
             *   - Cleaning.
             *   - Maintenance.
             *   - Repair.
             * - A subscription category, for example:
             *   - Monthly subscription.
             *   - Yearly subscription.
             *   - Lifetime subscription.
             * - A membership category, for example:
             *   - Basic membership.
             *   - Premium membership.
             *   - Gold membership.
             * - A partnership category, for example:
             *   - Silver partnership.
             *   - Gold partnership.
             *   - Platinum partnership.
             */
            $table->string('name')->unique();
            $table->text('description');

            $table->foreignId('parent_id')
                ->nullable()
                ->constrained('categories')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('category_type_id')
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
