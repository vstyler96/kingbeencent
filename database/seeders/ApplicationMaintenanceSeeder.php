<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;

class ApplicationMaintenanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
    }

    /**
     * Install the application maintenance data.
     */
    public function installPassportclients(): void
    {
        if (DB::table("oauth_clients")->count() > 0) return;

        $client = new ClientRepository;

        $passwordGrant = $client->createPasswordGrantClient(
            null,
            "Password Grant Client",
            sprintf("%s", config("app.url")),
            "users"
        );

        $personalAccess = $client->createPersonalAccessClient(
            null,
            "Personal Access Client",
            sprintf("%s", config("app.url")),
        );

        info("Access Clients", [
            "Personal access client" => $personalAccess->only("id", "secret"),
            "Password grant client"  => $passwordGrant->only("id", "secret"),
        ]);
    }
}
