<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::group([
    "name" => "seo",
], function () {

    Route::get('{any?}', [Controllers\SeoController::class, 'index'])
        ->where("any", ".*")
        ->name("index");
});
