<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Laravel\Cashier\Http\Middleware\VerifyRedirectUrl;
use Laravel\Cashier\Http\Middleware\VerifyWebhookSignature;

Route::post('login', [Controllers\UserController::class, 'login']);
Route::post('register', [Controllers\UserController::class, 'register']);
Route::get('application-settings', [Controllers\SettingsController::class, 'index']);
Route::post('stripe/webhook', [Controllers\PaymentController::class, 'webhook'])->middleware([
    VerifyRedirectUrl::class,
    VerifyWebhookSignature::class,
]);

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [Controllers\UserController::class, 'logout']);
    Route::get('me', [Controllers\UserController::class, 'me']);
    Route::put('user/password', [Controllers\UserController::class, 'updatePassword']);
    Route::apiResource('user.shipping-address', Controllers\UserShippingAddressContoller::class);

    // Files
    Route::get('file', [Controllers\FileController::class, 'index']);
    Route::post('file', [Controllers\FileController::class, 'store']);

    // Products
    Route::post('payment/intent', [Controllers\PaymentController::class, 'intent']);
    Route::post('payment/callback', [Controllers\PaymentController::class, 'callback']);

    // Route::post('payment/complete', [Controllers\PaymentController::class, 'completedPayment']);
    // Route::post('payment/failure', [Controllers\PaymentController::class, 'failedPayment']);
    Route::apiResource('product', Controllers\ProductController::class);
});
