<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Throwable;

class Image extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "hash",
        "path",
        "extension",
        "mime",
    ];

    /**
     * The attributes that should be appended to the model.
     *
     * @var array<int, string>
     */
    protected $appends = [
        "url",
    ];

    /**
     * Get the URL attribute.
     *
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return sprintf("/storage/%s", $this->path);
    }

    public static function booting()
    {
        static::deleting(function ($image) {
            try {
                Storage::delete("public/{$image->path}");
            } catch (Throwable $e) {
                logger()->channel("filesystem")->warning($e->getMessage(), $e->getTrace());
            }
        });
    }
}
