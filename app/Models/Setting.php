<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value',
        'key',
        'type',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getSetting($key)
    {
        return $this->where('key', $key)->first();
    }

    public function scopeAppSettings(Builder $query): array
    {
        return $query->whereNull('user_id')->get()->reduce(function($carry, $setting) {
            $value = $setting->value;
            @settype($value, $setting->type);
            return $carry + [ $setting->key => $value ?? null ];
        }, []);
    }

    public function scopeUserSettings(Builder $query): array
    {
        $user = me();

        if (is_null($user)) return [];

        return $query->where('user_id', $user->id)->get()->reduce(function($carry, $setting) {
            $value = $setting->value;
            @settype($value, $setting->type);
            return $carry + [ $setting->key => $value ?? null ];
        }, []);
    }
}
