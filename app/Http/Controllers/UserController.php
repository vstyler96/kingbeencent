<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    public function login(Request $request): JsonResponse
    {
        /** @var User|Authenticatable $user */
        $user = User::where('email', $request->input('email'))->first();

        $validated = $request->validate([
            'email'    => 'required|email',
            'password' => [
                'required',
                function ($attribute, $value, $fail) use ($user) {
                    if (!$user || !Hash::check($value, $user->password)) {
                        $fail("The provided :attribute does not match in our records.");
                    }
                },
            ],
        ]);

        $token = $user->buildToken();
        return response()->json([
            'token' => $token,
            'user'  => $user->load(['shippingAddresses'])->only('id', 'name', 'email', 'is_admin', 'shippingAddresses'),
        ]);
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function me(): JsonResponse
    {
        /** @var User|Authenticatable $user */
        $user = auth()->user();
        return response()->json($user->load(['shippingAddresses'])->only('id', 'name', 'email', 'is_admin', 'shippingAddresses'));
    }
}
