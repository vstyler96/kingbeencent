<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    public function __construct(
        public Setting $setting
    ) {
        //
    }

    /**
     * Display the main page.
     */
    public function index(): Renderable
    {
        return view("app", [
            "appSettings" => $this->setting->appSettings(),
            "userSettings" => $this->setting->userSettings(),
        ]);
    }
}
