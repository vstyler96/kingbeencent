<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Throwable;

class CustomValidations implements ValidationRule
{
    /**
     * Validate instance.
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        return;
    }

    /**
     * Ensure the composite uniqueness of multiple values on a DB Row.
     *
     * Called as: 'multi_composite_unique:model_name,key,value,key,value,null OR existing ID
     * Examples:
     * POST: 'white_only' => 'multi_composite_unique:\App\UseType,name,Storage,red_only,1,null
     * PUT:  'white_only' => 'multi_composite_unique:\App\UseType,name,Storage,red_only,1,5
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  array  $parameters
     *
     * @return bool
     */
    public function validateMultiUnique($attribute, $value, $parameters)
    {
        $model = array_shift($parameters);
        $exception = array_pop($parameters);
        $compositeItems = [];

        $compositeItems[$attribute] = $value;
        foreach (array_chunk($parameters, 2) as $pack) {
            $compositeItems[$pack[0]] = $pack[1];
        }

        $query = $model::query();

        foreach ($compositeItems as $k => $v) {
            if ($v === 0 || $v === '' || $v === null || !isset($v)) {
                $query->whereNull($k);
                continue;
            }

            $query->where($k, $v);
        }

        if (!is_null($exception)) {
            $query->where('id', '!=', $exception);
        }

        try {
            $query->firstOrFail();
            return false;
        } catch (Throwable) {
            // This passes validation, the composite unique col vals do not already exist on another model instance
            return true;
        }
    }
}
