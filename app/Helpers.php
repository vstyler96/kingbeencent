<?php

use App\Models\Scope;
use App\Models\User;

if (!function_exists('app_scopes')) {
    /**
     * Applications Scopes.
     */
    function app_scopes(): array
    {
        try {
            return Scope::all()->pluck('name', 'wildcard')->toArray();
        } catch (Exception) {
            return [
                "*"  => "Root Access",
                "admin" => "Admin Access",
                "user"  => "User Access",
            ];
        }
    }
}

if (!function_exists('multiCompositeRule')) {
    /**
     * Generate a Multi Composite unique Rule validation string.
     *
     * @param string $model
     * @param array $pairs
     * @param ?int $id
     *
     * @return string
     */
    function multiCompositeRule(
        string $model,
        array $pairs,
        ?int $id = null
    ): string {
        return sprintf(
            "multi_unique:%s,%s,%s",
            $model,
            implode(',', array_map(
                fn($k, $v) => sprintf("%s,%s", $k, $v ?? 'null'),
                array_keys($pairs),
                array_values($pairs)
            )),
            $id ?? 'null',
        );
    }
}

if (!function_exists("me")) {
    /**
     * Get the authenticated User.
     *
     * @return User
     */
    function me(): User|null
    {
        return auth('api')?->user();
    }
}
