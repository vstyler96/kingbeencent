<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class InvalidSettings extends Exception
{
    public function __construct(
        string $message = "",
        int $code = 422,
        ?Throwable $previous = null
    ) {
        parent::__construct(
            sprintf("Invalid Settings Provided. (Reason: %s)", $message),
            $code,
            $previous
        );
    }
}
