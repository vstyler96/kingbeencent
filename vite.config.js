import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';
import eslint from 'vite-plugin-eslint';

export default defineConfig({
  build: {
    chunkSizeWarningLimit: 2048,
  },
  plugins: [
    eslint(),
    vue({
      template: {
        transformAssetUrls: {
          base: null,
          // base: env.VITE_APP_URL,
          includeAbsolute: false,
        },
      },
    }),
    laravel({
      input: ['resources/js/app.js', 'resources/scss/app.scss'],
      refresh: true,
    }),
  ],
  publicDir: 'public',
  resolve: {
    alias: {
      '@': `${__dirname}/resources/js`,
    },
  },
});
