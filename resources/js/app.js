import { createApp } from 'vue';

import pinia from './store';
import router from './router';
// import vuetify from './vuetify';
import { Form, Field } from './veeValidate';
import ValidatedField from './components/form/ValidatedField.vue';
import ValidatedSelect from './components/form/ValidatedSelect.vue';
import ValidatedNumeric from './components/form/ValidatedNumeric.vue';
import './storage';
import './utils';

import Win from '7.css-vue';
import App from './Application.vue';

const app = createApp(App);

app.use(router);
app.use(pinia);
app.use(Win);
// app.use(vuetify);

app.component('VeeForm', Form);
app.component('VeeField', Field);

app.component('ValidatedField', ValidatedField);
app.component('ValidatedSelect', ValidatedSelect);
app.component('ValidatedNumeric', ValidatedNumeric);

app.mount('#app');
