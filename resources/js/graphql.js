import { GraphQLClient, gql } from 'graphql-request';

export { gql };
export async function request(query, variables) {
  const accessToken = localStorage.get('token.accessToken');
  const config = {};

  if (accessToken) {
    config.headers = {
      Authorization: `Bearer ${accessToken}`,
    };
  }

  try {
    const client = new GraphQLClient('/graphql', config);
    const data = await client.request(query, variables);
    return [data, null];
  } catch ({ response, response: { errors: [{ message }] } }) {
    return [null, { message, type: 'warning' }];
  }
}
