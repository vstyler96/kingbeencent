import numbro from 'numbro';

Number.prototype.toCurrency = function () {
  return numbro(this).formatCurrency({ mantissa: 2 });
};
