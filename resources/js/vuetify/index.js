// Vuetify
import { createVuetify } from 'vuetify';
import { md3 as blueprint } from 'vuetify/blueprints';

// Components and directives
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import * as labs from 'vuetify/labs/components';
import { es } from 'vuetify/locale';

// Styles
import '@mdi/font/css/materialdesignicons.css';
import './styles.scss';

// Themes
import themes from './themes';

// Custom Icon Set
import { si } from './simpleIcons';
// import { fa } from './faIcons';

export default createVuetify({
  blueprint,
  components: {
    ...components,
    ...labs,
  },
  directives,
  locale: {
    locale: 'es',
    fallback: 'en',
    messages: { es },
  },
  theme: {
    defaultTheme: 'light',
    variations: {
      colors: ['primary', 'secondary'],
    },
    themes,
  },
  icons: {
    sets: { si },
  },
  display: {
    mobileBreakpoint: 'sm',
  },
});
