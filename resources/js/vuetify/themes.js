const light = {
  dark: false,
  colors: {
    primary: '#5022b4',
    secondary: '#f59180',
    // success: '#27ae60',
    // info: '#2980b9',
    warning: '#e67e22',
    // error: '#c0392b',
  },
};

const dark = {
  dark: true,
  colors: {
    primary: '#00DEC8',
    secondary: '#FFB2B7',
  },
};

const themes = { light, dark };

export { light, dark };
export default themes;
