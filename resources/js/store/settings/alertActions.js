import { uniqueId } from 'lodash';

export default {
  appendAlert({ message, type }) {
    const id = uniqueId();

    this.alerts.push({
      id,
      message,
      type,
      visible: false,
    });

    setTimeout(() => {
      const index = this.alerts.findIndex(alert => alert.id === id);
      this.alerts[index].visible = true;
    }, 100);
    setTimeout(() => { this.removeAlert(id) }, 3000);
  },
  errorAlert(message) {
    this.appendAlert({
      message,
      type: 'error',
    });
  },
  infoAlert(message) {
    this.appendAlert({
      message,
      type: 'info',
    });
  },
  successAlert(message) {
    this.appendAlert({
      message,
      type: 'success',
    });
  },
  warningAlert(message) {
    this.appendAlert({
      message,
      type: 'warning',
    });
  },
  removeAlert(id) {
    const index = this.alerts.findIndex(alert => alert.id === id);
    if (index < 0) return;
    this.alerts[index].visible = false;
    setTimeout(() => { this.alerts.splice(index, 1) }, 300);
  },
};
