import { defineStore } from 'pinia';
import { request } from '@/axios';
import alertActions from './alertActions.js';

export default defineStore('settings', {
  state() {
    return {
      alerts: [],
      settings: {
        application: localStorage.get('appSettings', {}),
        cashier: {},
      },
    };
  },
  actions: {
    ...alertActions,
    async fetchAppSettings() {
      // Check if the appSettings object is not empty
      if (Object.keys(this.settings.application).length > 0) return;

      const [settings, error] = await request({
        url: 'application-settings',
        method: 'GET',
      });

      if (error) return;

      this.settings.application = settings;
      localStorage.put('appSettings', settings);
    },
  },
});
