import { createPinia } from 'pinia';
// import useAuthStore from './auth.js';
// import useBrandStore from './brand.js';
// import useFileStore from './file.js';
// import useOrderStore from './order.js';
// import useProductStore from './product.js';
// import useSettingsStore from './settings';
// import useUserStore from './user.js';

// export {
//   useAuthStore,
//   useBrandStore,
//   useFileStore,
//   useOrderStore,
//   useProductStore,
//   useSettingsStore,
//   useUserStore
// };

export default createPinia();
