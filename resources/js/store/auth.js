import { defineStore } from 'pinia';
import { request } from '@/axios';
import useSettingsStore from './settings/index.js';

export default defineStore('auth', {
  state() {
    return {
      credentials: {
        email: null,
        password: null,
      },
      registrar: {
        name: null,
        email: null,
        password: null,
      },
      errors: null,
      loading: false,
      token: localStorage.get('token', null),
      user: null,
      shippingAddress: null,
    };
  },
  getters: {
    shippingAddresses() {
      return this.user?.shippingAddresses || [];
    },
  },
  actions: {
    async login() {
      try {
        this.loading = true;
        const [response,,, errors] = await request({
          url: 'login',
          method: 'post',
          data: this.credentials,
        });

        if (errors) {
          this.errors = errors;
          return;
        }

        this.user = response.user;
        this.token = response.token;

        localStorage.set('token', response.token);
        return;
      } finally {
        this.loading = false;
      }
    },
    async logout() {
      try {
        this.loading = true;
        await request({
          url: 'logout',
          method: 'post',
        });

        this.user = null;
        this.token = null;

        localStorage.removeItem('token');
        localStorage.removeItem('cart');
      } finally {
        this.loading = false;
      }
    },
    async register() {
      const { errorAlert, successAlert } = useSettingsStore();
      try {
        this.loading = true;
        const [response, message,, errors] = await request({
          url: 'register',
          method: 'post',
          data: this.registrar,
        });

        if (errors) {
          this.errors = errors;
          errorAlert(message);
          return;
        }

        successAlert(`¡Te damos la bienvenida, ${this.registrar.name}!`);

        this.user = response.user;
        this.token = response.token;

        localStorage.set('token', response.token);
        return;
      } finally {
        this.loading = false;
      }
    },
    async me() {
      if (!this.token || this.user?.id > 0) return;

      try {
        this.loading = true;
        const [user, error, code] = await request({ url: 'me' });

        if (error || !user) {
          if ([422, 401, 403].includes(code)) {
            localStorage.removeItem('token');
            localStorage.removeItem('cart');
            this.token = null;
            return;
          }
          return;
        }

        this.user = user;
      } finally {
        this.loading = false;
      }
    },
    async updatePassword(data) {
      const { errorAlert, successAlert } = useSettingsStore();
      const [, message,, errors] = await request({
        url: '/user/password',
        method: 'PUT',
        data,
      });

      if (message) {
        errorAlert(message);
        this.errors = errors;
        return;
      }

      successAlert('Contraseña actualizada!');
      this.errors = null;
    },
    async fetchShippingAddresses() {
      const { user } = this;
      if (!user) return;
      const [response, message] = await request({
        url: `/user/${user.id}/shipping-address`,
      });

      if (message) return;

      this.user.shippingAddresses = response;
    },
    async createShippingAddress(data) {
      const { errorAlert, successAlert } = useSettingsStore();
      const { user } = this;

      const [, message,, errors] = await request({
        url: `/user/${user.id}/shipping-address`,
        method: 'POST',
        data,
      });

      if (message) {
        errorAlert(message);
        this.errors = errors;
        return;
      }

      successAlert('Dirección de envío agregada!');
      this.fetchShippingAddresses();
      this.errors = null;
    },
    async updateShippingAddress(address) {
      const { errorAlert, successAlert } = useSettingsStore();
      const { user } = this;
      const { id, ...data } = address;

      const [, message,] = await request({
        url: `/user/${user.id}/shipping-address/${id}`,
        method: 'PUT',
        data,
      });

      this.fetchShippingAddresses();

      if (message) {
        errorAlert(message);
        return;
      }

      successAlert('Dirección de envío predeterminada!');
    },
  },
});
