import useAuthStore from '@/store/auth.js';

function intersect(array1, array2) {
  return (array1 || []).filter(value => array2.includes(value));
}

const components = {
  drawer: () => import('../components/Drawer.vue'),
};

const beforeEnter = async to => {
  const { token: { scopes: tokenScopes } } = useAuthStore();
  const { meta: { scopes: expectedScopes } } = to;

  if (!intersect(tokenScopes, expectedScopes)) return { name: 'profile' };

  return true;
}

const appSections = [
  {
    name: 'dashboard.products',
    path: 'products',
    component: () => import('@/app/products/index.vue'),
    meta: {
      title: 'Productos',
      icon: 'mdi-storefront',
      scopes: ['*', 'admin'],
    },
    beforeEnter,
  },
  {
    name: 'dashboard.orders',
    path: 'orders',
    component: () => import('@/app/Dashboard.vue'),
    meta: {
      title: 'Compras',
      icon: 'mdi-cart-arrow-up',
      scopes: ['*', 'admin'],
    },
    beforeEnter,
  },
  {
    name: 'dashboard.users',
    path: 'users',
    component: () => import('@/app/Dashboard.vue'),
    meta: {
      title: 'Usuarios',
      icon: 'mdi-account-group',
      scopes: ['*'],
    },
    beforeEnter,
  },
];

export { appSections };
export default {
  components: {
    default: () => import('../app/index.vue'),
    ...components,
  },
  path: '/dashboard',
  meta: {
    title: 'Panel de Control',
    icon: 'mdi-view-dashboard',
  },
  children: [
    {
      name: 'dashboard',
      path: '',
      component: () => import('@/app/Dashboard.vue'),
      meta: {
        title: 'Panel de Control',
        icon: 'mdi-view-dashboard',
        scopes: ['*', 'admin'],
      },
    },
    ...appSections,
  ],
};
