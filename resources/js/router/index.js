import { createRouter, createWebHistory } from 'vue-router';
import useAuthStore from '@/store/auth.js';
// import useSettingsStore from '@/store/settings/index.js';
import routes from './routes';

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach(async to => {
  document.title = `${import.meta.env.VITE_APP_NAME} :: ${to.meta.title}`;

  const auth = useAuthStore();
  // const { fetchAppSettings } = useSettingsStore();

  await auth.me();

  if (to?.meta?.public) return true;

  if (!to.meta?.public && !auth.user?.id) {
    return { name: 'welcome' };
  }

  return true;
});

export default router;
