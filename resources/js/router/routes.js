// import dashboard from './dashboard';
// import userSections from './user';

// const components = {
//   drawer: () => import('../components/Drawer.vue'),
//   footer: () => import('../components/Footer.vue'),
// };

// const store = {
//   path: '/store',
//   name: 'store.all',
//   components: {
//     default: () => import('../views/Collections/All.vue'),
//     ...components,
//   },
//   meta: {
//     title: 'Tienda',
//     icon: 'mdi-storefront',
//     public: true,
//   },
// };

// const contact = {
//   path: '/contact',
//   name: 'contact',
//   components: {
//     default: () => import('../views/Contact.vue'),
//     ...components,
//   },
//   meta: {
//     title: 'Contacto',
//     icon: 'mdi-email',
//     public: true,
//   }
// };

// const routes = [
//   {
//     path: '/',
//     alias: '/welcome',
//     name: 'welcome',
//     components: {
//       default: () => import('../views/Welcome.vue'),
//       ...components,
//     },
//     meta: {
//       title: 'Inicio',
//       icon: 'mdi-home',
//       public: true,
//     }
//   },
//   store,
//   contact,
// ];

// const footer = [
//   store,
//   contact,
//   {
//     path: '/ship-policies',
//     name: 'ship-policies',
//     components: {
//       default: () => import('../views/Welcome.vue'),
//       ...components,
//     },
//     meta: {
//       title: 'Políticas de entrega y devolución',
//       icon: 'mdi-package-up',
//       public: true,
//     }
//   },
// ];

// export {

// };

export default [
  {
    path: '/',
    alias: '/welcome',
    name: 'welcome',
    component: () => import('@/views/Welcome.vue'),
    meta: {
      title: 'Inicio',
      icon: 'mdi-home',
      public: true,
    },
  },
  // ...routes,
  // {
  //   name: 'product',
  //   path: '/product/:id',
  //   components: {
  //     default: () => import('../views/Product.vue'),
  //     ...components,
  //   },
  //   meta: {
  //     title: 'Product',
  //     icon: 'mdi-hanger',
  //     public: true,
  //   },
  // },
  // {
  //   path: '/collections',
  //   components: {
  //     default: () => import('../views/Collections/index.vue'),
  //     ...components,
  //   },
  //   children: [
  //     {
  //       path: ':store',
  //       name: 'collections.store',
  //       component: () => import('../views/Collections/Store.vue'),
  //     },
  //   ],
  // },
  // ...footer,
  // dashboard,
  // ...userSections,
  // {
  //   path: '/privacy-policy',
  //   name: 'privacy-policy',
  //   components: {
  //     default: () => import('../views/Welcome.vue'),
  //     ...components,
  //   },
  //   meta: {
  //     title: 'Política de Privacidad',
  //     icon: 'mdi-vpn',
  //     public: true,
  //   }
  // },
  // {
  //   path: '/service-terms',
  //   name: 'service-terms',
  //   components: {
  //     default: () => import('../views/Welcome.vue'),
  //     ...components,
  //   },
  //   meta: {
  //     title: 'Términos y Condiciones',
  //     icon: 'mdi-file-sign',
  //     public: true,
  //   }
  // },
];
