const components = {
  drawer: () => import('../components/Drawer.vue'),
  footer: () => import('../components/Footer.vue'),
};

export default [
  {
    path: '/cart',
    name: 'cart',
    components: {
      default: () => import('../app/Cart.vue'),
      ...components,
    },
    meta: {
      title: 'Carrito de Compras',
      icon: 'mdi-cart',
    },
  },
  {
    path: '/history',
    name: 'history',
    components: {
      default: () => import('../app/History/index.vue'),
      ...components,
    },
    meta: {
      title: 'Historial de Compras',
      icon: 'mdi-shopping',
    },
  },
  {
    path: '/profile',
    name: 'profile',
    components: {
      default: () => import('../app/Profile/index.vue'),
      ...components,
    },
    meta: {
      title: 'Perfil',
      icon: 'mdi-account-circle',
    },
  },
];
