import {
  Form,
  Field,
  defineRule,
  // configure,
} from 'vee-validate';
import { all as rules } from '@vee-validate/rules';
// import { localize } from '@vee-validate/i18n';
import PhoneNumber from 'awesome-phonenumber';
// import en from '@vee-validate/i18n/dist/locale/en.json';
// import es from '@vee-validate/i18n/dist/locale/es.json';

// configure({
//   generateMessage: localize({
//     es: {
//       code: 'es',
//       messages: {
//         alpha: 'Este campo solo debe contener letras',
//         alpha_dash: 'Este campo solo debe contener letras, números y guiones',
//         alpha_num: 'Este campo solo debe contener letras y números',
//         alpha_spaces: 'Este campo solo debe contener letras y espacios',
//         between: 'Este campo debe estar entre 0:{min} y 1:{max}',
//         confirmed: 'Este campo no coincide',
//         digits: 'Este campo debe ser numérico y contener exactamente 0:{length} dígitos',
//         dimensions: 'Este campo debe ser de 0:{width} píxeles por 1:{height} píxeles',
//         email: 'Este campo debe ser un correo electrónico válido',
//         not_one_of: 'Este campo debe ser un valor válido',
//         ext: 'Este campo debe ser un archivo válido',
//         image: 'Este campo debe ser una imagen',
//         one_of: 'Este campo debe ser un valor válido',
//         integer: 'Este campo debe ser un entero',
//         length: 'El largo deste campo debe ser 0:{length}',
//         max: 'Este campo no debe ser mayor a 0:{length} caracteres',
//         max_value: 'Este campo debe de ser 0:{max} o menor',
//         mimes: 'Este campo debe ser un tipo de archivo válido',
//         min: 'Este campo debe tener al menos 0:{length} caracteres',
//         min_value: 'Este campo debe ser 0:{min} o superior',
//         numeric: 'Este campo debe contener solo caracteres numéricos',
//         regex: 'El formato deste campo no es válido',
//         required: 'Este campo es obligatorio',
//         required_if: 'Este campo es obligatorio',
//         size: 'Este campo debe ser menor a 0:{size}KB',
//       },
//       names: {
//         name: 'nombre',
//         email: 'correo eletrónico',
//         description: 'descripción',
//         price: 'precio',
//         quantity: 'cantidad',
//         stock: 'cantidad',
//         category: 'categoría',
//         brand: 'marca',
//         image: 'imagen',
//         password: 'contraseña',
//         confirmation: 'confirmación',
//       },
//     }
//   }),
// });

Object.entries(rules).forEach(([rule, validator]) => {
  defineRule(rule, validator);
});

defineRule('decimal', value => {
  if (!value || !value.length) return true;
  if (!/^(-)?[0-9]+(\.[0-9]{1,8})?$/.test(value)) return 'Este campo debería ser un número decimal.';
  return true;
});

defineRule('phone', value => {
  const phone = new PhoneNumber(value);
  if(phone.isValid()) {
    return true;
  }

  return 'This field is not a valid phone number'
})

export { Form, Field };
export default { Form, Field };
