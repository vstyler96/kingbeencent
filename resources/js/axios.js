import axios from 'axios';

const client = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
  },
});

export async function request(config = {}) {
  try {
    config.baseURL = config?.baseURL || '/api';
    config.withCredentials = true;
    const token = localStorage.get('token.accessToken');

    if (token) {
      config.headers = {
        ...(config?.headers || {}),
        Authorization: `Bearer ${token}`,
      };
    }

    const { data } = await client.request(config);

    return [data, null];
  } catch (e) {
    if (Object.keys(e).includes('response')) {
      const {
        response: {
          status: code,
          data: {
            message,
            errors,
          } = {},
        },
      } = e;
      return [null, message, code, errors];
    }

    return  [null, e];
  }
}
