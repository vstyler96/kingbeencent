/**
 * Put a new object item on the localStorage and return the recent created one item.
 *
 * @param {String} key
 * @param {*} object
 *
 * @returns {Object}
 */
Storage.prototype.put = function (key, object) {
  this.setItem(key, JSON.stringify(object));
  return this.get(key);
};
Storage.prototype.set = Storage.prototype.put;

/**
 * Get an existing item from localStorage.
 *
 * @param {String} key
 *
 * @returns {*}
 */
Storage.prototype.get = function (key, fallback = undefined) {
  const [root, ...rest] = key.split('.');

  // If only root was requested...
  if (!rest?.length) {
    return JSON.parse(this.getItem(root)) || fallback;
  }

  return rest.reduce((carry, fragment) => carry && carry[fragment], JSON.parse(this.getItem(root))) || fallback;
};

/**
 * Get an existing item from localStorage and delete the index.
 *
 * @param {string} key
 *
 * @returns {*}
 */
Storage.prototype.pull = function (key) {
  const aux = this.get(key);
  this.remove(key);
  return aux;
};

/**
 * Delete an object from localStorage.
 *
 * @param {String} key
 *
 * @returns {Boolean}
 */
Storage.prototype.remove = function (key) {
  this.removeItem(key);
  return !(`${key}` in this);
};

/**
 * An alias for `clear()`, for cleaning the session.
 */
Storage.prototype.flush = localStorage.clear;
