<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ config("app.name") }}</title>

    {{-- <script src="https://js.stripe.com/v3/"></script> --}}
    <link rel="shortcut icon" href="{{ asset("/images/favicon.png") }}" type="image/png">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;500;700&family=Poppins:wght@100;300;400;500;700&display=swap" rel="stylesheet">

    @vite("resources/scss/app.scss")
</head>
<body>
    <div id="app"></div>

    <!-- Vite assets -->
    @vite("resources/js/app.js")
</body>
</html>
